package com.esewa.springbeanscope;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.FileSystemResource;

public class Main {
    public static void main(String[] args) {

        // Using scope="Singleton"
        // Two object displaying  same result
        BeanFactory beanFactory=new XmlBeanFactory(new FileSystemResource("src/resource/applicationContext.xml"));
        Student student=(Student)beanFactory.getBean("studentbean");
        student.display();

        Student student1=(Student)beanFactory.getBean("studentbean");
        student1.display();
        System.out.println();


        //Using scope="Prototype"
        //Two object displaying different result
        MessageDemo message=(MessageDemo) beanFactory.getBean("messagebean");
        message.setMessage("Hello");
        System.out.println(message.getMessage());


        MessageDemo message1=(MessageDemo) beanFactory.getBean("messagebean");
        System.out.println(message1.getMessage());

    }
}





